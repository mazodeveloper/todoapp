//
//  IconCell.swift
//  TODO-APP
//
//  Created by mazodirty on 4/27/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class IconCell: UITableViewCell {

    @IBOutlet weak var nameIcon: UILabel!
    
    @IBOutlet weak var imageIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
