//
//  AllListsVC.swift
//  TODO-APP
//
//  Created by mazodirty on 4/26/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class AllListsVC: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    //var lists = [ListOfItems]()
    var dataModel: DataModel!
    
    struct IDS {
        static let cell = "ListsCell"
        static let newListSegue = "NewListSegue"
        static let editListSegue = "EditListSegue"
        static let showChecklist = "ShowChecklist"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        //tableView.rowHeight = UITableViewAutomaticDimension
        //tableView.estimatedRowHeight = tableView.rowHeight
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.delegate = self
        
        let index = UserDefaults.standard.integer(forKey: "IndexListItems")
        if index >= 0 && index < dataModel.lists.count {
            let indexPath = IndexPath(row: index, section: 0)
            let cell = tableView.cellForRow(at: indexPath)
            performSegue(withIdentifier: IDS.showChecklist, sender: cell)
        }
        
    }
    
    //se ejecutara para traer las listas este init puede fallar
    /*required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadLists()
    }
    
    func loadLists() {
        let filePath = getFilePath()
        if let fileData = try? Data(contentsOf: filePath) {
            let unarchiver = NSKeyedUnarchiver(forReadingWith: fileData)
            lists = unarchiver.decodeObject(forKey: "Lists") as! [ListOfItems]
            unarchiver.finishDecoding()
        }
    }*/
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let list = dataModel.lists[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: IDS.cell, for: indexPath) as! ListsCell
        cell.nameList.text = list.name
        let uncheckedItems = list.countUncheckedItems()
        if uncheckedItems > 0 {
            cell.uncheckedItems.text = "\(uncheckedItems) Items remaining"
        }else {
            cell.uncheckedItems.text = "All done"
        }
        
        cell.iconList.image = UIImage(named: list.iconName)
        
        return cell
    }
    
    
    //Configuracion segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ListDetailVC {
            vc.delegate = self
            
            if segue.identifier == IDS.editListSegue {
                let cell = sender as! ListsCell
                if let indexPath = tableView.indexPath(for: cell) {
                    vc.listToEdit = dataModel.lists[indexPath.row]
                }                
            }
            
        }
        
        if segue.identifier == IDS.showChecklist {
            if let vc = segue.destination as? ChecklistVC {
                
                let cell = sender as! ListsCell
                if let indexPath = tableView.indexPath(for: cell) {
                    vc.list = dataModel.lists[indexPath.row]
                    vc.delegate = self
                    UserDefaults.standard.set(indexPath.row, forKey: "IndexListItems")
                }
            }
        }
        
        
    }
    
}


//MARK: -> UITableViewDelegate
extension AllListsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Eliminar") {[unowned self] (action, indexPath) in
            
            self.dataModel.lists.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            
            self.dataModel.saveLists()
        }
        
        return [deleteAction]
    }
    
}

//MARK: -> Create and update lists
extension AllListsVC: ListDetailVCDelegate {
    
    func listDetailDelegate(_ controller: ListDetailVC, finishAdding list: ListOfItems) {
    
        dataModel.lists.append(list)
        dataModel.sortLists()
        
        if let row = dataModel.lists.index(of: list) {
            let indexPath = IndexPath(row: row, section: 0)
            tableView.insertRows(at: [indexPath], with: .fade)
            
            dataModel.saveLists()
        }
    }
    
    func listDetailDelegate(_ controller: ListDetailVC, finishEditing list: ListOfItems) {
        
        
        if let index = dataModel.lists.index(of: list) {
            let indexPath = IndexPath(row: index, section: 0)
            if let cell = tableView.cellForRow(at: indexPath) {
                let celda = cell as! ListsCell
                celda.nameList.text = list.name
                celda.iconList.image = UIImage(named: list.iconName)
                
                dataModel.sortLists()
                if let newIndex = dataModel.lists.index(of: list){
                    let newIndexPath = IndexPath(row: newIndex, section: 0)
                    tableView.moveRow(at: indexPath, to: newIndexPath)
                }
            }
            
            dataModel.saveLists()
        }
    }
    
    
    //save lists if a file
    /*func getMainDirectory() -> URL {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[0]
    }
    
    func getFilePath() -> URL {
        let path = getMainDirectory().appendingPathComponent("lists.plist")
        print(path)
        return path
    }
    
    func saveLists() {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.encode(lists, forKey: "Lists")
        archiver.finishEncoding()
        data.write(to: getFilePath(), atomically: true)
    }*/
    
}


//MARK: -> UINavigationControllerDelegate
//nos permite identificar cuando el usuario a precionado el boton back y demas opciones
extension AllListsVC: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController === self {
            UserDefaults.standard.set(-1, forKey: "IndexListItems")
        }
    }
}


//MARK: -> CheckedItemsDelegate
extension AllListsVC: ChecklistVCDelegate {
    
    func checklistDelegate(_ controller: ChecklistVC, finishProcess list: ListOfItems) {
        
        if let index = dataModel.lists.index(of: list) {
            let indexPath = IndexPath(row: index, section: 0)
            if let cell = tableView.cellForRow(at: indexPath) {
                let celda = cell as! ListsCell
                let uncheckedItems = list.countUncheckedItems()
                if uncheckedItems > 0 {
                    celda.uncheckedItems.text = "\(uncheckedItems) Items remaining"
                }else {
                    celda.uncheckedItems.text = "All done"
                }
                
            }
        }
    }
    
    
}












