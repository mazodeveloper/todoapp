//
//  ChecklistItem.swift
//  TODO-APP
//
//  Created by mazodirty on 4/24/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import Foundation
import UserNotifications

class ChecklistItem: NSObject, NSCoding {
    
    var itemText: String!
    var itemChecked: Bool!
    var date = Date()
    var shouldRemind = false
    var itemID: Int
    
    init(itemText: String, itemChecked: Bool) {
        self.itemText = itemText
        self.itemChecked = itemChecked
        self.itemID = DataModel.nextItemID()
        super.init()
    }
    
    deinit {
        deleteNotification()
    }
    
    required init?(coder aDecoder: NSCoder) {
        itemText = aDecoder.decodeObject(forKey: "Text") as! String
        itemChecked = aDecoder.decodeObject(forKey: "Checked") as! Bool
        date = aDecoder.decodeObject(forKey: "Date") as! Date
        shouldRemind = aDecoder.decodeBool(forKey: "ShouldRemind")
        itemID = aDecoder.decodeInteger(forKey: "ItemID")
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(itemText, forKey: "Text")
        aCoder.encode(itemChecked, forKey: "Checked")
        aCoder.encode(date, forKey: "Date")
        aCoder.encode(shouldRemind, forKey: "ShouldRemind")
        aCoder.encode(itemID, forKey: "ItemID")
    }
    
    func deleteNotification() {
        
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: ["\(itemID)"])
        
    }
    
    func programarNotificacion() {
        
        deleteNotification()
        if shouldRemind && date > Date() {
            
            //create notification
            let content = UNMutableNotificationContent()
            content.title = "Reminder: "
            content.body = itemText
            content.sound = UNNotificationSound.default()
            
            let calendar = Calendar(identifier: .gregorian)
            let components = calendar.dateComponents([.month, .day, .hour, .minute], from: date)
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
            
            let request = UNNotificationRequest(identifier: "\(itemID)", content: content, trigger: trigger)
            
            let center = UNUserNotificationCenter.current()
            center.add(request)
            
            print("Make new notification \(request) for the item \(itemID)")
        }
    }
    
}














