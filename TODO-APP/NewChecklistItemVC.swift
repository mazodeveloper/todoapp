//
//  NewChecklistItemVC.swift
//  TODO-APP
//
//  Created by mazodirty on 4/24/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

protocol NewChecklistItemVCDelegate: class {
    func addItemDidCancel(_ controller: NewChecklistItemVC)
    func addItem(_ controller: NewChecklistItemVC, didFinishAdding item: ChecklistItem)
    func editItem(_ controller: NewChecklistItemVC, didFinishEditing item: ChecklistItem)
}

class NewChecklistItemVC: UIViewController {

    @IBOutlet weak var newTaskTextfield: UITextField!
    @IBOutlet weak var remindMeSwitch: UISwitch!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var dateItem = Date()
    var editItem: ChecklistItem?
    weak var delegate: NewChecklistItemVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let item = editItem {
            navigationItem.title = "Edit item"
            newTaskTextfield.text = item.itemText
            remindMeSwitch.isOn = item.shouldRemind
            dateItem = item.date
            
            if remindMeSwitch.isOn {
                datePicker.isHidden = false
                datePicker.setDate(dateItem, animated: false)
            }else {
                datePicker.isHidden = true
            }
        }
        
        updateDateLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if remindMeSwitch.isOn {
            newTaskTextfield.resignFirstResponder()
        }else {
            newTaskTextfield.becomeFirstResponder()
        }
    }
    
    private func updateDateLabel() {
        let format = DateFormatter()
        format.dateStyle = .medium
        format.timeStyle = .short
        dateLabel.text = format.string(from: dateItem)
    }
    
    
    private func showDatePicker() {
        datePicker.isHidden = false
    }
    
    //Switch action
    @IBAction func remindMeAction(_ sender: UISwitch) {
        if sender.isOn {
            newTaskTextfield.resignFirstResponder()
            showDatePicker()
        }else {
            datePicker.isHidden = true
        }
    }
    
    //Datepicker action
    @IBAction func changeDate(_ sender: UIDatePicker) {
        dateItem = sender.date
        updateDateLabel()
    }
    
    @IBAction func validateNewTask() {
        
        if newTaskTextfield.text == "" {
            showAlertMessage(title: "Has olvidado rellenar el campo", message: "El campo de la nueva tarea es obligatorio.")
            return
        }
        
        newTaskTextfield.resignFirstResponder()
        
        if editItem == nil {
            //new item
            let item = ChecklistItem(itemText: newTaskTextfield.text!, itemChecked: false)
            item.date = dateItem
            item.shouldRemind = remindMeSwitch.isOn
            
            //realizar notificacion
            item.programarNotificacion()
            
            delegate?.addItem(self, didFinishAdding: item)
        }else {
            
            var makeNotification = true
            if editItem?.shouldRemind == remindMeSwitch.isOn && editItem?.date == dateItem {
                makeNotification = false
            }
            
            editItem?.itemText = newTaskTextfield.text!
            editItem?.shouldRemind = remindMeSwitch.isOn
            if remindMeSwitch.isOn {
                editItem?.date = dateItem
            }
            
            //validar si debemos notificar
            if makeNotification {
                editItem?.programarNotificacion()
            }
            
            delegate?.editItem(self, didFinishEditing: editItem!)
        }
        
        navigationController?.popViewController(animated: true)
        
        //pasar data por medio de unwind segue
        //performSegue(withIdentifier: "UnwindToChecklist", sender: nil)
    }
    
    
    private func showAlertMessage(title: String, message: String) {
    
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let close = UIAlertAction(title: "Cerrar", style: .cancel, handler: nil)
        
        alert.addAction(close)
        present(alert, animated: true, completion: nil)
    }
    
}
