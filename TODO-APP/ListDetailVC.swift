//
//  ListDetailVC.swift
//  TODO-APP
//
//  Created by mazodirty on 4/26/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

protocol ListDetailVCDelegate: class {
    
    func listDetailDelegate(_ controller: ListDetailVC, finishAdding list: ListOfItems)
    func listDetailDelegate(_ controller: ListDetailVC, finishEditing list: ListOfItems)
}

class ListDetailVC: UITableViewController {
    
    @IBOutlet weak var nameList: UITextField!
    var listToEdit: ListOfItems?
    var delegate: ListDetailVCDelegate?
    
    @IBOutlet weak var iconName: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    struct IDS {
        static let chooseIcon = "ChooseIconSegue"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameList.becomeFirstResponder()
        if listToEdit != nil {
            nameList.text = listToEdit?.name
            iconName.text = listToEdit?.iconName
            iconImage.image = UIImage(named: (listToEdit?.iconName)!)
            
            navigationItem.title = "Edit list"
        }
    }
    
    @IBAction func createUpdateList(_ sender: Any) {
        
        if nameList.text == "" {
            showAlert(title: "Has olvidado un campo", message: "El campo nombre de la lista es obligatorio")
            return
        }
        
        if iconName.text == "" || iconName.text == "Name icon"{
            showAlert(title: "Has olvidado el icono", message: "Debes seleccionar un icono para la lista.")
            return
        }
        
        if let listToEdit = listToEdit {
            listToEdit.name = nameList.text!
            listToEdit.iconName = iconName.text!
            delegate?.listDetailDelegate(self, finishEditing: listToEdit)
        }else {
            let list = ListOfItems(name: nameList.text!)
            list.iconName = iconName.text!
            delegate?.listDetailDelegate(self, finishAdding: list)
        }
        
        nameList.resignFirstResponder()
        navigationController?.popViewController(animated: true)
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let close = UIAlertAction(title: "Cerrar", style: .cancel, handler: nil)
        alert.addAction(close)
        
        present(alert, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.section == 1 {
            return indexPath
        }else {
            return nil
        }
    }
    
}


//MARK: -> IconPickerVCDelegate
extension ListDetailVC: IconPickerVCDelegate {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == IDS.chooseIcon {
            let vc = segue.destination as! IconPickerVC
            vc.delegate = self
        }
        
    }
    
    func iconPickerDelegate(_ controller: IconPickerVC, didPick iconName: String) {
        self.iconName.text = iconName
        iconImage.image = UIImage(named: iconName)
    }
    
}









