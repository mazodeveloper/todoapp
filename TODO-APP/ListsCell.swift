//
//  ListsCell.swift
//  TODO-APP
//
//  Created by mazodirty on 4/26/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class ListsCell: UITableViewCell {
    
    @IBOutlet weak var nameList: UILabel!
    
    @IBOutlet weak var uncheckedItems: UILabel!
    
    @IBOutlet weak var iconList: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
