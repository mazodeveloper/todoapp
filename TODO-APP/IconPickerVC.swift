//
//  IconPickerVC.swift
//  TODO-APP
//
//  Created by mazodirty on 4/27/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

protocol IconPickerVCDelegate: class {
    func iconPickerDelegate(_ controller: IconPickerVC, didPick iconName: String)
}

class IconPickerVC: UITableViewController {
    
    weak var delegate: IconPickerVCDelegate?
    
    struct IDS {
        static let iconCell = "IconCell"
    }
    
    let icons = [
        "No Icon",
        "Appointments",
        "Birthdays",
        "Chores",
        "Drinks",
        "Folder",
        "Groceries",
        "Inbox",
        "Photos",
        "Trips"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

//MARK: -> UITableViewDatasource
extension IconPickerVC {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return icons.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: IDS.iconCell, for: indexPath) as! IconCell
        
        let icon = icons[indexPath.row]
        cell.nameIcon.text = icon
        cell.imageIcon.image = UIImage(named: icon)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let icon = icons[indexPath.row]
        delegate?.iconPickerDelegate(self, didPick: icon)
        
        navigationController?.popViewController(animated: true)
    }
    
}












