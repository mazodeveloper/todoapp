//
//  ListOfItems.swift
//  TODO-APP
//
//  Created by mazodirty on 4/26/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import Foundation

class ListOfItems: NSObject, NSCoding {
    
    var name: String!
    var items = [ChecklistItem]()
    var iconName: String!
    
    init(name: String) {
        self.name = name
        iconName = "Appointments"
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObject(forKey: "Name") as! String
        items = aDecoder.decodeObject(forKey: "Items") as! [ChecklistItem]
        iconName = aDecoder.decodeObject(forKey: "IconName") as! String
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "Name")
        aCoder.encode(items, forKey: "Items")
        aCoder.encode(iconName, forKey: "IconName")
    }
    
    func countUncheckedItems() -> Int {
        
        var count = 0
        for item in items where !item.itemChecked {
            count += 1
        }
        
        return count
    }
    
}







