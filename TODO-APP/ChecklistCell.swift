//
//  ChecklistCell.swift
//  TODO-APP
//
//  Created by mazodirty on 4/24/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

class ChecklistCell: UITableViewCell {
    
    @IBOutlet weak var itemText: UILabel!
    
    @IBOutlet weak var checkedLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
