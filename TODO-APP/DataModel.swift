//
//  DataModel.swift
//  TODO-APP
//
//  Created by mazodirty on 4/26/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import Foundation

class DataModel {
    
    var lists = [ListOfItems]()
    
    init() {
        loadLists()
        registerDefaults()
    }
    
    func loadLists() {
        let filePath = getFilePath()
        if let fileData = try? Data(contentsOf: filePath) {
            let unarchiver = NSKeyedUnarchiver(forReadingWith: fileData)
            lists = unarchiver.decodeObject(forKey: "Lists") as! [ListOfItems]
            unarchiver.finishDecoding()
            sortLists()
        }
    }
    
    func getMainDirectory() -> URL {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[0]
    }
    
    func getFilePath() -> URL {
        let path = getMainDirectory().appendingPathComponent("lists.plist")
        print(path)
        return path
    }
    
    func saveLists() {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.encode(lists, forKey: "Lists")
        archiver.finishEncoding()
        data.write(to: getFilePath(), atomically: true)
    }
    
    //Este memtodo configurara los valores por defecto de UserDefaults
    func registerDefaults() {
        let dictionary: [String: Any] = [
            "IndexListItems": -1,
            "FirstTime": true,
            "currentItemID": 0
        ]
        UserDefaults.standard.register(defaults: dictionary)
    }
    
    //Organizar las listas alfabeticamente
    func sortLists() {
        lists.sort(by: {$0.name.localizedStandardCompare($1.name) == .orderedAscending} )
    }
    
    //cada vez que se cree un nuevo item retornamos un id unico
    class func nextItemID() -> Int{
        
        let currentItemID = UserDefaults.standard.integer(forKey: "currentItemID")
        UserDefaults.standard.set(currentItemID + 1, forKey: "currentItemID")
        UserDefaults.standard.synchronize()
        
        return currentItemID
    }
    
}



















