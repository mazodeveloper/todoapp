//
//  ViewController.swift
//  TODO-APP
//
//  Created by mazodirty on 4/24/17.
//  Copyright © 2017 mazodirty. All rights reserved.
//

import UIKit

protocol ChecklistVCDelegate: class {
    func checklistDelegate(_ controller: ChecklistVC, finishProcess list: ListOfItems)
}

class ChecklistVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var list: ListOfItems!
    
    weak var delegate: ChecklistVCDelegate?
    
    struct IDS {
        static let cell = "ChecklistCell"
        static let newTaskSegue = "AddNewTaskSegue"
        static let editItemSegue = "EditItemSegue"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        if list != nil {
            navigationItem.title = list.name
        }
    }
    
    /*required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadChecklistItems()
    }*/
    
    //los carga desde el archivo .plist
    /*func loadChecklistItems() {
        let filePath = getFilePath()
        if let fileData = try? Data(contentsOf: filePath) {
            
            let unarchiver = NSKeyedUnarchiver(forReadingWith: fileData)
            items = unarchiver.decodeObject(forKey: "ChecklistItems") as! [ChecklistItem]
            unarchiver.finishDecoding()
        }
    }*/
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: IDS.cell, for: indexPath) as! ChecklistCell
        let item = list.items[indexPath.row]
        cell.itemText.text = item.itemText
        
        if item.itemChecked {
            cell.checkedLabel.text = "✓"
        }else {
            cell.checkedLabel.text = ""
        }
        
        return cell
    }

    
    //Manejo de segue's
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == IDS.newTaskSegue || segue.identifier == IDS.editItemSegue{
            let vc = segue.destination as! NewChecklistItemVC
            vc.delegate = self
            
            if segue.identifier == IDS.editItemSegue {
                
                if let index = tableView.indexPath(for: sender as! ChecklistCell) {
                    vc.editItem = list.items[index.row]
                }
                
            }
            
        }
    }
    
    
    //Agregar un item por medio de unwind segue
    /*@IBAction func unwindAddItem(segue: UIStoryboardSegue) {
        
        if segue.identifier == "UnwindToChecklist" {
            let newItemVC = segue.source as! NewChecklistItemVC
            let textItem = newItemVC.newTaskTextfield.text
            
            if let textItem = textItem {
                let item = ChecklistItem(itemText: textItem, itemChecked: false)
                let row = items.count
                items.append(item)
                
                let indexPath = IndexPath(row: row, section: 0)
                let indexPaths = [indexPath]
                
                tableView.insertRows(at: indexPaths, with: .fade)
            }
        }
        
    }*/
    
}

// MARK: -> UITableViewDelegate
extension ChecklistVC {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) {
            let celda = cell as! ChecklistCell
            if celda.checkedLabel.text == "✓" {
                list.items[indexPath.row].itemChecked = false
                celda.checkedLabel.text = ""
            }else {
                list.items[indexPath.row].itemChecked = true
                celda.checkedLabel.text = "✓"
            }
            
            delegate?.checklistDelegate(self, finishProcess: list)
            //saveCheckListItems()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Eliminar") { [unowned self] (rowAction, indexPath) in
            
            let item = self.list.items[indexPath.row]
            self.list.items.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            
            if !item.itemChecked {
                self.delegate?.checklistDelegate(self, finishProcess: self.list)
            }
            
            //self.saveCheckListItems()
        }
        
        return [deleteAction]
    }
    
}

//MARK: -> AddItemDelegate
extension ChecklistVC: NewChecklistItemVCDelegate {
    
    func addItemDidCancel(_ controller: NewChecklistItemVC) {
    }
    
    func addItem(_ controller: NewChecklistItemVC, didFinishAdding item: ChecklistItem) {
        let row = list.items.count
        list.items.append(item)
        let indexPath = IndexPath(row: row, section: 0)
        
        delegate?.checklistDelegate(self, finishProcess: list)
        
        tableView.insertRows(at: [indexPath], with: .fade)
        
        //saveCheckListItems()
    }
    
    func editItem(_ controller: NewChecklistItemVC, didFinishEditing item: ChecklistItem) {
        
        if let index = list.items.index(of: item) {
            if let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) {
                let checklistCell = cell as! ChecklistCell
                checklistCell.itemText.text = item.itemText
            }
            
            //saveCheckListItems()
        }
    }
    
}


//MARK: -> Save items in a file .plist
/*extension ChecklistVC {
    
    func getMainDirectory() -> URL {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let url = urls[0]
        
        return url
    }
    
    func getFilePath() -> URL {
        return getMainDirectory().appendingPathComponent("Checklists.plist")
    }
    
    func saveCheckListItems() {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.encode(items, forKey: "ChecklistItems")
        archiver.finishEncoding()
        data.write(to: getFilePath(), atomically: true)
    }
 
}*/

















